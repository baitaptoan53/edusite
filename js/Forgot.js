var forEmail = document.querySelector('#forEmail');
var verifi = document.querySelector('#Verification');
var newPassword = document.querySelector('#newPassword');
var enterNewPassword = document.querySelector('#enterNewPassword');
var fromForgot = document.querySelector('#from_forgot');

//all
function showError(input, message) {
    let parent = input.parentElement;
    let span = parent.querySelector('span');

    span.innerText = message;
    span.classList.add('text-danger');
}

function showSuccess(input) {
    let parent = input.parentElement;
    let span = parent.querySelector('span');

    span.innerText = '';
    span.classList.remove('text-danger');
}
//all

function checkErrorAll(listInput) {
    listInput.forEach(input => {
        input.value = input.value.trim();
        if(input.value == ''){
            showError(input, 'Không được để trống');
        }
        else {
            showSuccess(input);
        }
    })
}
function checkLength(input, min, max) {
    input.value = input.value.trim();

    if(input.value.length < min){
        showError(input, `Có ít nhất ${min} ký tự`);
    }
    if(input.value.length > max){
        showError(input, `Có nhiều nhất ${max} kí tự`);
    }
}
function testEmail(input) {
    let regexEmail =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

    input.value = input.value.trim();
    if(regexEmail.test(input.value)){
        showSuccess(input);
    }
    else
    {
        showError(input, 'Invalid email');
    }
   
}
function testTwoPass(pass, enterPass){
    if(pass.value != enterPass.value){
        showError(enterPass, 'Mật khẩu không trùng khớp')
    }
}
fromForgot.onsubmit = function(e) {
    e.preventDefault();
    let isErrorAll = checkErrorAll([forEmail, verifi, newPassword, enterNewPassword]);
    let isForEmailError = testEmail(forEmail);
    let isForEmailLength = checkLength(forEmail, 15, 60);
    let isNewPassLength = checkLength(newPassword,5,20);
    let isEnterNewPassLength = checkLength(enterNewPassword,5,20);
    let isTestPass = testTwoPass(newPassword,enterNewPassword);
}