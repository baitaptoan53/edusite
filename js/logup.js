var fullName = document.querySelector('#exampleInputFullName');
var email = document.querySelector('#exampleInputEmail');
var password = document.querySelector('#exampleInputPassword');
var enterPassword = document.querySelector('#enterInputPassword');
var fromUp = document.querySelector('#form_up');

//all
function showError(input, message) {
    let parent = input.parentElement;
    let span = parent.querySelector('span');

    span.innerText = message;
    span.classList.add('text-danger');
}

function showSuccess(input) {
    let parent = input.parentElement;
    let span = parent.querySelector('span');

    span.innerText = '';
    span.classList.remove('text-danger');
}
//all

function checkErrorAll(listInput) {
    let error = false;
    listInput.forEach(input => {
        input.value = input.value.trim();
        if(input.value == ''){
            error = true;
            showError(input, 'Không được để trống');
        }
        else {
            showSuccess(input);
        }
    })
    return error;
}
function checkLength(input, min, max) {
    input.value = input.value.trim();


    if(input.value.length < min){
        showError(input, `Có ít nhất ${min} ký tự`);
        return true;
    }
    if(input.value.length > max){
        showError(input, `Có nhiều nhất ${max} kí tự`);
        return true;
    }
    return false;
}
function testEmail(input) {
    let regexEmail =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

    input.value = input.value.trim();
    if(regexEmail.test(input.value)){
        showSuccess(input);
        return false;
    }
    else
    {
        showError(input, 'Invalid email');
        return false;
    }
   return regexEmail.test(input.value);
}
function testTwoPass(pass, enterPass){
    if(pass.value != enterPass.value){
        showError(enterPass, 'Mật khẩu không trùng khớp')
        return true;
    }
    return false;
}
fromUp.onsubmit = function(e) {
    e.preventDefault();
    let isErrorAll = checkErrorAll([fullName, email, password, enterPassword]);
    let isEmailError = testEmail(email);
    let isFullNameLength = checkLength(fullName, 15, 40);
    let isEmailLength = checkLength(email, 15, 60);
    let isPassLength = checkLength(password,5,20);
    let isEnterPassLength = checkLength(enterPassword,5,20);
    let isTestPass = testTwoPass(password,enterPassword);

    if(isErrorAll || isEmailError || isFullNameLength || isEmailLength || isPassLength || isEnterPassLength ||isTestPass){
        //no đc làm gì
    }
    else
    {
        var user = {
            fullName: fullName.value,
            email: email.value,
            password: password.value,
        }
       // console.log(user)
        var json = JSON.stringify(user);
        //console.log(json)
        var inJson =  localStorage.setItem(email.value,json);
        alert("Đăng ký thành công")
        window.location.href = "./login.html";
    }
}