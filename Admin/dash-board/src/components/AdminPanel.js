import * as React from "react";
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from "chart.js";
ChartJS.register(ArcElement, Tooltip, Legend);
import { Doughnut, Pie } from "react-chartjs-2";
import LocalOfferIcon from "@mui/icons-material/LocalOffer";
import TrendingDownIcon from "@mui/icons-material/TrendingDown";
import TrendingUpIcon from "@mui/icons-material/TrendingUp";
import SsidChartIcon from "@mui/icons-material/SsidChart";
import MenuBookIcon from "@mui/icons-material/MenuBook";
import HomeIcon from "@mui/icons-material/Home";
import { Card, ListGroup } from "react-bootstrap";
export const data = {
  labels: [
    "Course HTML CSS",
    "Course JS",
    "Course Reactjs",
    "Course PHP",
    "Course Laravel",
    "Course NodeJS",
  ],
  datasets: [
    {
      label: "# of Votes",
      data: [190, 140, 100, 50, 20, 110],
      backgroundColor: [
        "rgba(255, 99, 132, 0.2)",
        "rgba(54, 162, 235, 0.2)",
        "rgba(255, 206, 86, 0.2)",
        "rgba(75, 192, 192, 0.2)",
        "rgba(153, 102, 255, 0.2)",
        "rgba(255, 159, 64, 0.2)",
      ],
      borderColor: [
        "rgba(255, 99, 132, 1)",
        "rgba(54, 162, 235, 1)",
        "rgba(255, 206, 86, 1)",
        "rgba(75, 192, 192, 1)",
        "rgba(153, 102, 255, 1)",
        "rgba(255, 159, 64, 1)",
      ],
      borderWidth: 1,
    },
  ],
  options: {
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
          },
        },
      ],
    },
  },
};
export const data2 = {
  labels: [
    "Sunday",
    "Saturday",
    "Friday",
    "Thursday",
    "Wednesday",
    "Tuesday",
    "Monday",
  ],
  datasets: [
    {
      label: "# of Votes",
      data: [12, 19, 3, 5, 2, 3, 5],
      backgroundColor: [
        "rgba(255, 99, 132, 0.2)",
        "rgba(54, 162, 235, 0.2)",
        "rgba(255, 206, 86, 0.2)",
        "rgba(75, 192, 192, 0.2)",
        "rgba(153, 102, 255, 0.2)",
        "rgba(255, 159, 64, 0.2)",
        "rgba(52, 45, 113, .2)",
      ],
      borderColor: [
        "rgba(255, 99, 132, 1)",
        "rgba(54, 162, 235, 1)",
        "rgba(255, 206, 86, 1)",
        "rgba(75, 192, 192, 1)",
        "rgba(153, 102, 255, 1)",
        "rgba(255, 159, 64, 1)",
        "rgba(52, 45, 113, 1)",
      ],
      borderWidth: 1,
    },
  ],
};

function AdminPanel() {
  return (
    <div>
      <div className="container mt-4 ">
        <h2 className="mb-3">
          <i className="me-2">
            <HomeIcon />
          </i>
          Dashboard
        </h2>
        <div className="mb-4 row justify-content-between text-center">
          <div
            className="col-md-3 rounded mb-4"
            style={{
              background: "linear-gradient(to right, #a8ff78, #78ffd6)",
            }}
          >
            <p>
              Total Tales
              <i className="ms-3">
                <LocalOfferIcon />
              </i>
            </p>
            <h3>
              $320,210 <TrendingDownIcon />
            </h3>
          </div>
          <div
            className="col-md-3 rounded mb-4"
            style={{
              backgroundColor: " #0093E9",
              background: "linear-gradient(160deg, #0093E9 0%, #80D0C7 100%)",
            }}
          >
            <p>
              Total Profit
              <i className="ms-3">
                <SsidChartIcon />
              </i>
            </p>
            <h3>
              $30,210 <TrendingDownIcon />
            </h3>
          </div>
          <div
            className="col-md-3 rounded mb-4"
            style={{
              background: "linear-gradient(to right, #9796f0, #fbc7d4)",
            }}
          >
            <p>
              Total Orders
              <i className="ms-3">
                <MenuBookIcon />
              </i>
            </p>
            <h3>
              610 <TrendingUpIcon />
            </h3>
          </div>
        </div>
        <div className="row">
          <div className="col-md-6">
            <h2 className="text-center ">Sales by Course</h2>
            <div className="col-md-12">
              <Doughnut data={data} />
            </div>
          </div>
          <div className="col-md-6">
            <h2 className="text-center">Sales of Course for the week</h2>
            <div className="col-md-12">
              <Pie data={data2} />
            </div>
          </div>
        </div>
        <div className="row mt-4 justify-content-around">
          <Card className="col-md-5">
            <Card.Header className="text-center">
              <h4> Business situation report </h4>
            </Card.Header>
            <Card.Body>
              One of the financial statements is the balance sheet. It shows an
              entity's assets, liabilities, and stockholders' equity as of the
              report date. In this report, the total of all assets must match
              the combined total of all liabilities and equity. The asset
              information on the balance sheet is subdivided into current and
              long-term assets. Similarly, the liability information is
              subdivided into current and long-term liabilities. This
              stratification is useful for determining the liquidity of a
              business. Ideally, the total of all current liabilities should
              exceed the total of all current liabilities, which implies that a
              business has sufficient assets to pay off its current obligations.
              The balance sheet is also used to compare debt levels to the
              amount of equity invested in the business, to see if its leverage
              level is appropriate.
            </Card.Body>
          </Card>
          <Card className="col-md-5">
            <Card.Header className="text-center">
              <h4> Courses </h4>
            </Card.Header>
            <ListGroup variant="flush">
              <ListGroup.Item>HTML,Css</ListGroup.Item>
              <ListGroup.Item>JS</ListGroup.Item>
              <ListGroup.Item>ReactJs</ListGroup.Item>
              <ListGroup.Item>PHP</ListGroup.Item>
              <ListGroup.Item>Laravel</ListGroup.Item>
              <ListGroup.Item>NodeJs</ListGroup.Item>
            </ListGroup>
          </Card>
        </div>
      </div>
    </div>
  );
}
export default AdminPanel;
