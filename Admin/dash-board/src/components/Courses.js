import {
  List,
  Datagrid,
  TextField,
  Edit,
  SimpleForm,
  EditButton,
  TextInput,
  Create,
  DeleteButton,
  SearchInput,
} from "react-admin";
const postFilters = [
  <SearchInput source="id" alwaysOn />
];
export const listCourses = (props) => (
  <List {...props} filters={postFilters}>
    <h2 className="mb-2">
      <i className="me-2 p-1"></i>
      Courses list
    </h2>
    <Datagrid>
      <TextField source="id" />
      <TextField source="name" />
      <TextField source="price" />
      <TextField source="description" />

      <EditButton basePath="/courses" />
      <DeleteButton basePath="/courses" />
    </Datagrid>
  </List>
);

export const editCourses = (props) => (
  <Edit {...props}>
  <h2 className="mb-2">
      <i className="me-2 p-1"></i>
      Courses Edit
    </h2>
    <SimpleForm>
      <TextInput source="name" />
      <TextInput source="price" />
      <TextInput source="description" />
    </SimpleForm>
  </Edit>
);

export const createCourses = (props) => (
  <Create {...props}>
  <h2 className="mb-2">
      <i className="me-2 p-1"></i>
      Courses Create
    </h2>
    <SimpleForm>
      <TextInput source="name" />
      <TextInput source="price" />
      <TextInput source="description" />
    </SimpleForm>
  </Create>
);
